
document.addEventListener("DOMContentLoaded", () => {
    init();
});
function init() {
    if (localStorage.getItem("theme")) {
        document.documentElement.setAttribute("theme", "dark");
    } else {
        document.documentElement.removeAttribute("theme");
    }
}
const changeBtn = document.querySelector("#change-theme");
changeBtn.addEventListener("click", function () {
    if (document.documentElement.hasAttribute("theme")) {
        document.documentElement.removeAttribute("theme");
        localStorage.removeItem("theme");
    } else {
        document.documentElement.setAttribute("theme", "dark");
        localStorage.setItem("theme", 1);
    }
});








// document.addEventListener("DOMContentLoaded", () => {
//     init();
// });
//
// function init() {
//     if (localStorage.getItem("theme") === "dark") {
//         document.body.setAttribute("theme", "dark");
//     } else {
//         document.body.removeAttribute("theme");
//     }
// }
//
// const changeBtn = document.querySelector("#change-theme");
// changeBtn.addEventListener("click", function () {
//     if (document.body.hasAttribute("theme")) {
//         document.body.removeAttribute("theme");
//         localStorage.removeItem("theme");
//     } else {
//         document.body.setAttribute("theme", "dark");
//         localStorage.setItem("theme", "dark");
//     }
// });





